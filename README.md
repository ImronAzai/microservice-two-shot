# Wardrobify

Team:

* Adam Azai - Hats Microservice
* John Agni - Shoes microservice?

## Design

## Shoes microservice
Django and React based web application which manages an inventory of shoes.

### BinVO Model:
- represents a copy of the Bin model in Wardrobe microservice in which shoes are stored in
- Identified by unique import_href used to import data
- Includes closet name, bin number, and bin size that the user can create
- Create a bin using POST: http://localhost:8100/api/bins using
JSON:
 {
    "closet_name": (string for closet name),
    "bin_number": (a number for bin number),
    "bin_size: (a number for bin size)
}
- See list of bins using GET: http://localhost:8100/api/bins
- Used to display data we poll from Bin model of Wardrobe



### Shoe Model:
- Represents shoes in the system
- includes fields for the shoe manufacturer, model name, color, picture, and bin they're in.
- References the BinVo Model, meaning it is a many-to-one relationship, there can be many shoes in one bin
- Create a pair of shoes using GET: http://localhost:8080/api/shoes/ using
 JSON:
 {
    "manufacturer": "string for manufacturer name",
    "model_name": "string for model name",
    "color": "string for color",
    "picture_url": "url for picture",
    "bin": "string for bin name"
}

### Polling:
- Periodically fetches data from wardrobe API and updates the BinVO model
- getBins() fetches the data and updates or creates a new BinVO object continuously with poll() loop with a delay of 60 seconds between each iteration
- ensures BinVO model is synchronized with data from wardrobe API



## Hats microservice

Hats_rest is the Django application for the Django project Hat. The models present in my hats_rest application are a
Hat model and a LocationVO model.

The Hat model is a simple model that contain the fields that describes the property of a hat along with an url field to populate with an image url of the said hat. The style_name field pertaining to the type of hat (flatback, cap, baseball hat, fedora). Fabric field being the material the hat is composed of. Color field stating the color of the hat.

A foreign key named location exists in the Hat model. The location foreign key establishes a 1 to many relationship with there being one location having multiple hats. An example of this 1 to many relationship would be a closet containing 2 hats or a hat being within a dresser/garage storage. This foreign key allows us to display which location the hat is shown when we are gathering the details of one specific hat.

The LocationVO model is a copy of the Location model within the wardrobe microservice. The Location Value Object is used to display the data we poll from the Location model of Wardobe. The poll folder within hats is a polling service that pulls that data within the location model and updates the empty fields of our LocationVO with the fields containing the information on the whereabouts of a hat.

With the polling service gathering the location model information from the wardrobe microservice we establish a line of communication between the two microservice. This communication was necessary to achieve the required function listed in the guidelines of the hat card/table/list properly displaying the location(closet_name,section_number,shelf_number). Now the hat card can display its own properties as well as where it is located in your Wardrobe.
