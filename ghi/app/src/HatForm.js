import {React, useEffect, useState} from 'react';


function HatForm(props){
    const [locations, setLocations] = useState([]);
    const [styleName, setStyleName] = useState('');
    const [color, setColor] = useState('');
    const [fabric, setFabric] = useState('');
    const [picture, setPicture] = useState('');
    const [location, setLocation] = useState('');

    const handleSubmit= async(event) => {
        event.preventDefault()
        const data = {};
        data.style_name = styleName
        data.fabric = fabric
        data.color = color
        data.picture_url = picture
        data.location = location
        const selectIndex = document.getElementById('location').value
        const hatUrl = `http://localhost:8090${selectIndex}hats/`
        const fetchConfig = {
          method:'post',
          body:JSON.stringify(data),
          headers: {
            'Content-type':'application/json'
          },
        }
        const presentationResponse = await fetch(hatUrl, fetchConfig)
        if (presentationResponse.ok){
          const newPresentation = await presentationResponse.json()
          setStyleName('');
          setColor('');
          setFabric('');
          setPicture('');
          setLocation('');
        } else {

        }

    }

    const handleStyleNameChange = (event) => {
        const value = event.target.value
        setStyleName(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value);
    }

    const handleFabricChange = (event) => {
        const value = event.target.value
        setFabric(value);
    }

    const handlePictureChange = (event) => {
        const value = event.target.value
        setPicture(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value
        setLocation(value)
    }


    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);
            if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
            }
        }


        useEffect( () => {
          fetchData()
        }, []);


    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Hat</h1>
            <form  onSubmit={handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input onChange={handleStyleNameChange} value={styleName} placeholder="Style Name" required type="text" id="style_name"  name="style_name" className="form-control"/>
                <label htmlFor="style_name">Hat Style Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" id="color" name="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFabricChange} value={fabric}placeholder="Fabric" required type="text" id="fabric" name="fabric" className="form-control"/>
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePictureChange} value={picture} placeholder="Picture" required type="url" id="picture_url" name="picture_url" className="form-control"/>
                <label htmlFor="picture_url">Image</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} value={location} required id="location" name='location' className="form-select">
                  <option  value="">Choose a Location</option>
                    {locations.map(location => {
                      return (
                        <option key={location.href} value={location.href}>
                          {location.closet_name}
                        </option>
                      )
                    })}
                </select>
              </div>
              <button onSubmit={handleAlert} className="btn btn-primary" type="submit">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
    }
export default HatForm;
