import React from "react"


class ShoesForm extends React.Component { //Class component
    constructor (props) { //constructor creates and intializes state object
        super(props) //intialize prop parameter to parent class
        this.state = { //sets initial state
            manufacturer: "",
            model_name: "",
            color: "",
            picture_url: "",
            bin: "",
            bins: [],
            hasCreatedShoe: false,
        }
    }

    handleSubmit = async (event) => { //async so triggered once form is submitted
        event.preventDefault(); //prevents page refresh
        const data = {...this.state}; //create new object called data that copies current state of component
        delete data.bins; //removes bin property

        delete data.hasCreatedShoe; //removes hasCreatedShoe property

        const shoesUrl = "http://localhost:8080/api/shoes/";
        const fetchConfig = { //creats fetchConfig object
            method: "post", //sends post request
            body: JSON.stringify(data), //body is data object converted to JSON
            headers: {
                'Content-Type': 'application/json', //indicates content type is json
            },
        };
        const response = await fetch(shoesUrl, fetchConfig); //sends 'POST' request to shoesURL using fetch passing into fetchConfig

        if (response.ok) {
            const newShoe = await response.json(); //if ok code reads the response data using response.json
            const cleared = { //sets all properties to empty strings or true
                manufacturer: "",
                model_name: "",
                color: "",
                picture_url: "",
                bin: "",
                hasCreatedShoe: true,
            };
            this.setState(cleared); //update component with cleared data
            window.location.href="/shoes" //redirects user to /shoes page
        }
    }

    handleInputChange = (event) => { //update state component whenever there are form changes
        const name = event.target.name;
        const value = event.target.value;
        this.setState({...this.state, [name]: value}) //create wa new state object that includes previous state, sets new state
    }

    async componentDidMount() { //lifecycle method
        const url = "http://localhost:8100/api/bins/";
        const response = await fetch(url); //fetch a list of bins
        if (response.ok) {
            const data = await response.json();
            this.setState({bins: data.bins}) //sets bin property
        }
    }

    render() {
      let messageClasses = "alert alert-success d-none mb-0";
      let formClasses = "";
      if (this.state.hasCreatedShoe) {
        messageClasses = "alert alert-success mb-0"; //displays succes message if shoe created succesfully
        formClasses = "d-none"; //hide the form
      }

        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Add New Shoe</h1>
                <form onSubmit={this.handleSubmit} id="create-shoe-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleInputChange} value={this.state.manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                    <label htmlFor="manufacturer">Manufacturer</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleInputChange} value={this.state.model_name} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control"/>
                    <label htmlFor="model_name">Model Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleInputChange} value={this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                    <label htmlFor="color">Color</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleInputChange} value={this.state.picture_url} placeholder="Picture" required type="url" name="picture_url" id="picture_url" className="form-control"/>
                    <label htmlFor="picture_url">Picture</label>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleInputChange} value={this.state.bin} required id="bin" name="bin" className="form-select">
                      <option value="">Choose Bin</option>
                    {this.state.bins.map(bin => {
                        return (
                            <option key={bin.href} value={bin.href}>
                                {bin.closet_name}
                            </option>
                        )
                    })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
                <div>
              <p> </p>
            </div>
            <div className={messageClasses} id="success-message">
              Shoe successfully created!
            </div>
              </div>
            </div>
          </div>
        )
    }
}

export default ShoesForm;
