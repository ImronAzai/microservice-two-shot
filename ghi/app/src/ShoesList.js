import React, { useState, useEffect } from "react";
import "./index.css";

function ShoesList() { //functional component ShoesList
  const [shoes, setShoes] = useState([]); //useState hook to define state variable `shoes`, and update function `setShoes`
  const getShoes = async () => {
    const shoesUrl = `http://localhost:8080/api/shoes/`; //url is endpoint to fetch data from
    const response = await fetch(shoesUrl); // sends request to endpoint and waits for response
    if (response.ok) {
      const listShoes = await response.json(); //extract json data, assign to listShoes
      setShoes(listShoes.shoes); //update shoes state varaible with the data
    }
  };

  useEffect(() => { //fetch data to manipulate the DOM after component is mounted, guarantees component is mounted before data is fetched
    getShoes(); //fetches data from web api to update shoes state variable
  }, []); //empty array means call back will run once, shoes state variable updated once, preventing any unnecessary calls

  const deleteShoe = (id) => async () => { //takes id parameter, return async function (high order function)
    try { //Easier promise code
      const url = `http://localhost:8080/api/shoes/${id}/`; //delete request at this API endpoint
      const deleteResponse = await fetch(url, { //response is assigned to deleteResponse var
        method: "delete",
      });

      if (deleteResponse.ok) {
        const reloadUrl = `http://localhost:8080/api/shoes/`;
        const reloadResponse = await fetch(reloadUrl); //send another request to API endpoint to update data using fetch
        const newShoes = await reloadResponse.json(); //newly received data extracted using json
        setShoes(newShoes.shoes); //set function used to update variable with new data
      }
    } catch (err) {
        console.error(err) //display error if something goes wrong
    }
  };

  if (shoes === undefined) { //component will not render if data is unavailable
    return null;
  }

  return ( //JSX for ShoesList
    <>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model Name</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Bin</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
        {/* .map method used to iterate over each shoe in shoes array and render a table row for each one */}
          {shoes.map((shoe) => {
            return (
              <tr key={shoe.id}>
                <td>{shoe.manufacturer}</td>
                <td>{shoe.model_name}</td>
                <td>{shoe.color}</td>
                <td>
                  <img
                    src={shoe.picture_url} //resize picture
                    alt=""
                    width="100px"
                    height="100px"
                  />
                </td>
                <td>{shoe.bin}</td>
                <td>
                  <button onClick={deleteShoe(shoe.id)} className="btn btn-danger">
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default ShoesList;
