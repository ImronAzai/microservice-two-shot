import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import './index.css'



// function refreshPage() {
//     return ( async() => {
//     const url = 'http://localhost:8090/api/hats'
//     const refreshPage = await fetch('http://localhost:8090/api/hats')
//     if (refreshPage.ok){
//         const refresh = await refreshPage.json()
//     }
// }
//     )
// }




// function below is the formating of the Hat Card along with the details of a Hat along with
// its location details being passed in the different sections of the card
function Hat(props){

    return (
        <div className="card-columns">
            {props.list.map(data => {
                return (
                    <div key ={data.href} className="card mb-3 shadow"  >
                        <img src={data.picture_url}  height="100px"  className="card-img-top" />
                        <div className="card-body">
                            <h5 className ="card=title">{data.style_name}</h5>
                            <h6 className='card-subtitle mb-2 text-muted'>{data.color},{data.fabric}</h6>
                            <p className="card-text">
                                This hat's present location is at {data.location.closet_name} within section {data.location.section_number} on shelf {data.location.shelf_number}
                            </p>
                            <div className="text-center">
                                {/* Button below allows the card to be deleted */}
                            <button className="text-center btn btn-danger btn-sm"
                                onClick={async() =>{
                                    const hatDetailUrl= `http://localhost:8090${data.href}`
                                    const deleteResponse = await fetch(hatDetailUrl, { method: 'DELETE'})
                            if (deleteResponse.ok){
                                window.location.reload()
                                const deleteMessage = await deleteResponse
                            } else{
                                console.log('Deletion gone wrong')
                            }
                        }
                            }>
                                Delete Hat
                            </button>
                            </div>
                        </div>
                    </div>
                );
            })}
        </div>
    )
}
// function below is the organization of the card as well as the list of requests from the hat's details
// Esstenially checking if the response of getting the list of hats is ok, then making a list of requests
// to grab every hat that exists by its href and then placing each request into a card
// the promise all ensures that all the requests in the list are fufilled before any of the cards are shown
const HatCard = (props) =>  {
    const [hatColumns, setHatColumns] = useState([[], [], []]);

    const fetchData = async () => {
      const url = 'http://localhost:8090/api/hats';

      try {
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          const requests = [];
          for (let hat of data.hats) {
            const detailUrl = `http://localhost:8090${hat.href}`;
            requests.push(fetch(detailUrl));
          }
          const responses = await Promise.all(requests);
          const columns = [[], [], []];
          let i = 0;
          for (const hatResponse of responses) {
            if (hatResponse.ok) {
              const details = await hatResponse.json();
              columns[i].push(details);
              i = i + 1;
              if (i > 2) {
                i = 0;
              }
            } else {
              console.error(hatResponse);
            }
          }
          setHatColumns(columns);
        }
      } catch (e) {
        console.error(e);
      }
    }
    useEffect(() => {
      fetchData();
    }, []);
    return (
      <>
        <div className="container">
          <div className="row">
            {hatColumns.map((hatList, index) => {
              return (
                <Hat key={index} list={hatList} />
              );
            })}
          </div>
        </div>
      </>
    );
  }
  export default HatCard;
