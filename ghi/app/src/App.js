import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesForm from './ShoeForm';
import ShoesList from './ShoesList';
import React from 'react';
import Hat from './Hat';
import HatForm from './HatForm';


function App(props) {
  if (props.shoes === undefined) {
    return null;
  }


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          {/* Routes for my nav buttons that lead to jsx form/pages */}
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route index element={<ShoesList shoes={props.shoes} />} />
            <Route path="new" element={<ShoesForm />} />
          </Route>
          <Route path="hats">
            <Route path="" element={<Hat />} />
            <Route path='new' element={<HatForm/>}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}


export default App;
