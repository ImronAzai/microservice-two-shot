from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Hat, LocationVO

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        'section_number',
        "shelf_number",
        "import_href",
    ]


class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style_name",
    ]

class ShowHatEncoder(ModelEncoder):
    model=Hat
    properties = [
        'style_name',
        'fabric',
        'color',
        'picture_url',
        'location',
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }



@require_http_methods(['GET', 'POST'])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            location_href = f'/api/locations/{location_vo_id}/'
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hats = Hat.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=ShowHatEncoder,
            safe=False
        )

@require_http_methods(['GET','DELETE',])
def api_show_hats(request, pk):
    if request.method == "GET":
        hats = Hat.objects.get(pk=pk)
        return JsonResponse(
            hats,
            encoder=ShowHatEncoder,
            safe=False
        )
    else:
        count, _ = Hat.objects.filter(pk=pk).delete()
        return JsonResponse({"deleted": count > 0})
