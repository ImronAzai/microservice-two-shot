from django.db import models
from django.urls import reverse





class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()






class Hat(models.Model):

    style_name = models.CharField(max_length=100)
    fabric= models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(null=True, max_length=200)
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE
    )

    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"pk": self.pk})
    def __str__(self):
        return f'{self.style_name}'












# def get_api_url(self):
#         return reverse("api_bin", kwargs={"pk": self.pk})

#     def __str__(self):
#         return f"{self.closet_name} - {self.bin_number}/{self.bin_size}"

#     class Meta:
#         ordering = ("closet_name", "bin_number", "bin_size")
